export namespace Schema {

    export interface Options {

        interval?:number;
        verbose?:boolean;
        type:string[];
    }

    export interface Source {

        name:string;
        stub:string;
        profile_picture?:string;
        created_at?:string;
        url_ref?:string;
        url_feed?:string;

    }

    export interface Item {

        stub?:string;
        ref_item?:string;
        dt?:any;
        dt_post?:any;
        text?:string;
        source?:string; // stub of refered source
        meta?:string;
        link?:string;

    }

    export interface DebugObject {
        
        t:number;
        dt:number;
        created_at:number;
        media?:any;
        itemCount?:number;
        items?:string[];
        sources?:any[];

    }

    export interface ParamsGet {
        tweet_mode:string;
        screen_name?:string;
        list_id?:string;
    }

}