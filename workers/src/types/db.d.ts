export namespace iDB {

    export interface getOptions {
        limit:number;
    }

    export interface getRandomOptions extends getOptions {

    }

    export interface getSingleOptions {
        type?:string;
    }

}