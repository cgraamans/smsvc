import * as commandLineArgs from "command-line-args";

import * as twitter from 'twitter';

import {db} from "./lib/db";
import {Tools} from "./lib/tools";
import {Schema} from './types/schema'
import Twitter = require("twitter");
import ShortId = require("shortid");

let DebugObject:Schema.DebugObject = null;

const options = commandLineArgs([
        
    { name: 'verbose', alias: 'v', type: Boolean, defaultValue:true },
    { name: 'interval', alias: 'i', type: Number, defaultValue:30000 },
    { name: 'type', alias: 't', type: String, defaultValue:'twitter_list' }

]);

const startTime = (new Date().getTime());
if(options.verbose) DebugObject = {t:0,dt:startTime,created_at:startTime};

// Run function
const run = async (options:any) => {

    try {

        setInterval(async ()=>{

            if(options.verbose) DebugObject.dt = (new Date()).getTime();

            // calling tick function
            return await tick(options);

        },options.interval);

    } catch(e) {

        console.log(e);

    }

}

const getSourceStubByStub = async (stub:string) => {

    try {

        let DBSourceStubCheck = await db.q("SELECT stub FROM sources WHERE stub = $1",[stub]);
        
        if(DBSourceStubCheck.rows.length === 1) return stub;
        
        return;

    } catch(e) {

        throw e;

    }

};

const Source = async (tweet:any,options:any) => {

    try {

        let SourceByStub:string = await getSourceStubByStub(tweet.user.screen_name);
        if(!SourceByStub) {

            let SourceArray = [
                tweet.user.screen_name,
                (new Date().toISOString()),
                tweet.user.profile_image_url_https,
                options.media.id
            ];

            await db.q(`
                    INSERT INTO sources 
                    (stub,created_at,profile_picture,media_id) 
                    VALUES($1,$2,$3,$4)
                `,SourceArray);

            SourceByStub = tweet.user.screen_name;
            if(options.verbose) console.log('Source',tweet.user.screen_name);

        }

        return SourceByStub;

    } catch(e) {

        throw e;

    }

};

const SetItem = async (tweet:Twitter.ResponseData,options:any) => {

    try {

        let refItem:string = null;

        if(tweet.retweeted_status) {
            refItem = await SetItem(tweet.retweeted_status,options);
        }

        let ItemStubCheck = await db.q("SELECT stub FROM items WHERE text = $1 AND source = $2",[tweet.full_text,tweet.user.screen_name]);
        if(ItemStubCheck.rows.length > 0) return ItemStubCheck.rows[0].stub;

        let sourceStub:string = await Source(tweet,options);
        if(!sourceStub) throw 'no source';

        let stub = ShortId.generate();

        let ItemArray = [
            stub,
            sourceStub,
            refItem,
            (new Date().toISOString()),
            (new Date(tweet.created_at).toISOString()),
            tweet.full_text,
            tweet.id_str
        ];
        
        await db.q(`
                INSERT INTO items 
                (stub,source,ref_item,dt,dt_post,text,link) 
                VALUES($1,$2,$3,$4,$5,$6,$7)
            `,ItemArray);

        if(options.verbose) DebugObject.items.push(stub);

        return stub;

    } catch(e) {

        throw e;

    }

};

// Tick function
const tick = async (options:any) => {

    try {

        const client = new twitter({
            consumer_key: process.env.EUSPIDER_TW_C_KEY,
            consumer_secret: process.env.EUSPIDER_TW_C_SECRET,
            access_token_key: process.env.EUSPIDER_TW_AT_KEY,
            access_token_secret: process.env.EUSPIDER_TW_AT_SECRET
        });

        if(options.verbose) { 
            DebugObject.items = [],
            DebugObject.sources = [],
            DebugObject.media = null;
        };

        let media = await db.q("SELECT * from media WHERE type = $1 ORDER BY random() LIMIT 1",[options.type]);
        if(!media || !media.rows || media.rows.length < 1) return;

        const mediaItem = media.rows[0];
        if(!mediaItem) return;        
        
        options.media = mediaItem;
        if(options.verbose) DebugObject.media = mediaItem; 

        // Twitter Call
        let type = 'statuses/user_timeline';
        let params:Schema.ParamsGet = {
            tweet_mode:'extended'
        };
        if(options.type === 'twitter') {
            params.screen_name = mediaItem.reference;
        }
        if(options.type === 'twitter_list') {
            type = 'lists/statuses';
            params.list_id = mediaItem.reference;
        }
        let get = await client.get(type,params);
        
        if(options.verbose) DebugObject.itemCount = get.length;

        // Processing Tweets
        if(get.length < 1) return;

        await Tools.asyncForEach(get,async (tweet:Twitter.ResponseData)=>{

            await SetItem(tweet,options);

            return;

        }).catch(e=>{
            throw e;
        });

        if(options.verbose) {
            DebugObject.t = (new Date()).getTime() - DebugObject.dt;
            console.log('DEBUG OBJECT',DebugObject);   
        }

        return;

    } catch(e) {

        console.log(e);

    }

};

// Execution with command line arguments
run(options);