
export class ToolsFactory {
    
    // reference: https://codeburst.io/javascript-async-await-with-foreach-b6ba62bbf404
    async asyncForEach(array:any, callback:any) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    debounce(func:any, wait:number, immediate?:any) {
        var timeout:any;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

}

export const Tools = new ToolsFactory();