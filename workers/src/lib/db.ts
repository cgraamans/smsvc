import {Pool} from "pg";

export class DBFactory {

    private static instance:DBFactory;

    private Pool:Pool;

    constructor() {

        this.Pool = new Pool({
            database: process.env.SMSDATABASE,
            host: process.env.SMSHOST,
            password: process.env.SMSPASSWORD,
            user: process.env.SMSUSER
        });

        this.Pool.on('error', (err, client) => {
            console.error('Unexpected error on idle client', err)
            process.exit(-1)
        });

    }

    static getInstance() {
        if (!DBFactory.instance) {
            DBFactory.instance = new DBFactory();
            // ... any one time initialization goes here ...
        }
        return DBFactory.instance;

    }

    async q(sql:string,vals:any) {

        const client = await this.Pool.connect();
        try {

            const res = await client.query(sql,vals);
            return res;

        } finally {

            client.release();

        }

    }

}

export const db = DBFactory.getInstance();