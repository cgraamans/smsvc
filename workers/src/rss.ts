import * as commandLineArgs from "command-line-args";

import {db} from "./lib/db";
import {Tools} from "./lib/tools";
import {Schema} from './types/schema'
import * as rssConverter from "rss-converter";
import shortid = require("shortid");

let DebugObject:Schema.DebugObject = null;

const options = commandLineArgs([
        
    { name: 'verbose', alias: 'v', type: Boolean, defaultValue:true },
    { name: 'interval', alias: 'i', type: Number, defaultValue:30000 },

]);

const itemObjPrototype:Schema.Item = {stub:null,source:null,dt:null,dt_post:null,text:null}

const startTime = (new Date().getTime());
if(options.verbose) DebugObject = {t:0,dt:startTime,created_at:startTime,items:[],sources:[],media:null};

// Run function
const run = async (options:any) => {

    try {

        Tools.debounce(setInterval(async ()=>{

            DebugObject.dt = (new Date().getTime());
            // calling tick function
            return await tick(options);

        },options.interval),120000);

    } catch(e) {

        console.log(e);

    }

}

const SetItems = async (SourceStub:string,items:any[],options:any) => {

    try {

        await Tools.asyncForEach(items, async (item:any)=>{

            // check item by source stub, title
            let unique = await db.q("SELECT source FROM items WHERE text = $1 AND source = $2 LIMIT 1",[item.title,SourceStub]);
            if(unique.rows.length > 0) return;

            let itemObj = Object.assign(itemObjPrototype,{
                stub:shortid.generate(),
                source:SourceStub,
                text:item.title,
                link:item.link,
                dt: (new Date()).toISOString(),
                dt_post: (new Date()).toISOString(),
                meta:null,
            });

            if(item.pubDate) itemObj.dt_post = (new Date(item.pubDate)).toISOString()
            if(item.dc_date) itemObj.dt_post = (new Date(item.dc_date)).toISOString()
            if(item.description) itemObj.meta = item.description.replace(/(<([^>]+)>)/ig, "");

            await db.q(`INSERT INTO items 
                        (
                            stub,
                            source,
                            text,
                            link,
                            dt,
                            dt_post,
                            meta
                        ) 
                        VALUES($1,$2,$3,$4,$5,$6,$7)`,
                        [
                            itemObj.stub,
                            itemObj.source,
                            itemObj.text,
                            itemObj.link,
                            itemObj.dt,
                            itemObj.dt_post,
                            itemObj.meta
                        ]
                    );

            if(options.verbose) DebugObject.items.push(itemObj.stub);

            return;

        });

        return;

    } catch(e) {

        throw e;

    }

    // return;

};

// MAIN RSS FUNCTION
const GetRSSFeedItems = async (source:Schema.Source)=>{

    try {

        return await rssConverter.toJson(source.url_feed);

    } catch(e) {

        await db.q("UPDATE sources SET is_active = false WHERE stub = $1",[source.stub])
        console.log(`Disabled: ${source.url_feed}`);
        return;

    }

}

// Tick function
const tick = async (options:any) => {

    try {
        
        if(options.verbose) { 
            DebugObject.items = [],
            DebugObject.sources = [],
            DebugObject.media = null;
        };

        let media = await db.q("SELECT * from media WHERE type = 'rss' ORDER BY random() LIMIT 1",[]);
        if(!media || !media.rows || media.rows.length < 1) return;

        const mediaItem = media.rows[0];
        if(!mediaItem) return;        
        if(options.verbose) DebugObject.media = mediaItem; 

        let sources = await db.q("SELECT * from sources AS s WHERE s.media_id = $1 AND s.is_active = true ORDER BY random() LIMIT 5",[mediaItem.id]);
        if(options.verbose) sources.rows.forEach(source=>DebugObject.sources.push(source.stub));
        
        await Tools.asyncForEach(sources.rows, async (source:Schema.Source)=>{

            let feed = await GetRSSFeedItems(source);

            if(!feed || feed.items.length < 1) return;

            if(feed && feed.items) {

                await SetItems(source.stub,feed.items,options);

            }    

            return;

        }).catch(e=>{
            
            throw e;
        
        });

        if(options.verbose) {
            DebugObject.t = (new Date()).getTime() - DebugObject.dt;
            console.log('DEBUG OBJECT',DebugObject);   
        }
        
        return;

    } catch(e) {

        console.log(e);

    }

};

// Execution with command line arguments
run(options);