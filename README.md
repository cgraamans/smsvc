# socialMediaMiningService

(a.k.a.sMMS)

## Intro

SMMS is a social media content scraper for Twitter, reddit, youtube and instagram.

Features:  
  - Keyword filtering  
  - Location database  
  - Automatic push updates

## API commands

## Setup

Requirements:  
  - nodejs  
  - postgres

### Database

Location: `<repo location>/init/postgres`

### API

Location: `<repo location>/api`

Build and Run:  
```
  npm i
  npm run build
  node dist/src.js
```

### Workers

Location: `<repo location>/workers`

Build and Run:  
```
  npm i
  npm run build
  node dist/src.js
```