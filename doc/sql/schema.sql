--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.sources DROP CONSTRAINT item_source_to_media_id;
ALTER TABLE ONLY public.sources DROP CONSTRAINT sources_pkey;
ALTER TABLE ONLY public.source_locations DROP CONSTRAINT source_locations_pkey;
ALTER TABLE ONLY public.media DROP CONSTRAINT media_pkey;
ALTER TABLE ONLY public.items DROP CONSTRAINT items_pkey;
ALTER TABLE ONLY public.data_cities DROP CONSTRAINT data_cities_pkey;
ALTER TABLE public.media ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.data_cities ALTER COLUMN id DROP DEFAULT;
DROP TABLE public.sources;
DROP TABLE public.source_locations;
DROP SEQUENCE public.media_type_seq;
DROP SEQUENCE public.media_id_seq;
DROP TABLE public.media;
DROP TABLE public.items;
DROP SEQUENCE public.data_cities_id_seq;
DROP TABLE public.data_cities;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: data_cities; Type: TABLE; Schema: public; Owner: SMSvc
--

CREATE TABLE public.data_cities (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    name_ascii character varying(255) NOT NULL,
    name_alt text NOT NULL,
    lat numeric(12,8) NOT NULL,
    lon numeric(12,8) NOT NULL,
    country character(2) NOT NULL,
    pop integer NOT NULL,
    alt integer NOT NULL,
    tz character varying(64) NOT NULL
);


ALTER TABLE public.data_cities OWNER TO "SMSvc";

--
-- Name: data_cities_id_seq; Type: SEQUENCE; Schema: public; Owner: SMSvc
--

CREATE SEQUENCE public.data_cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_cities_id_seq OWNER TO "SMSvc";

--
-- Name: data_cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: SMSvc
--

ALTER SEQUENCE public.data_cities_id_seq OWNED BY public.data_cities.id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: SMSvc
--

CREATE TABLE public.items (
    stub character varying(32) NOT NULL,
    dt timestamp without time zone NOT NULL,
    text text,
    dt_post timestamp without time zone NOT NULL,
    meta text,
    link character varying(256) NOT NULL,
    ref_item character varying(32) DEFAULT NULL::character varying,
    source character varying(64) DEFAULT 'NULL::character varying'::character varying
);


ALTER TABLE public.items OWNER TO "SMSvc";

--
-- Name: media; Type: TABLE; Schema: public; Owner: SMSvc
--

CREATE TABLE public.media (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    type character varying(24) NOT NULL,
    reference text,
    stub character varying(32) DEFAULT 'NULL::character varying'::character varying NOT NULL
);


ALTER TABLE public.media OWNER TO "SMSvc";

--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: SMSvc
--

CREATE SEQUENCE public.media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.media_id_seq OWNER TO "SMSvc";

--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: SMSvc
--

ALTER SEQUENCE public.media_id_seq OWNED BY public.media.id;


--
-- Name: media_type_seq; Type: SEQUENCE; Schema: public; Owner: SMSvc
--

CREATE SEQUENCE public.media_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.media_type_seq OWNER TO "SMSvc";

--
-- Name: media_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: SMSvc
--

ALTER SEQUENCE public.media_type_seq OWNED BY public.media.type;


--
-- Name: source_locations; Type: TABLE; Schema: public; Owner: SMSvc
--

CREATE TABLE public.source_locations (
    source_stub character varying(128) NOT NULL
);


ALTER TABLE public.source_locations OWNER TO "SMSvc";

--
-- Name: sources; Type: TABLE; Schema: public; Owner: SMSvc
--

CREATE TABLE public.sources (
    stub character varying(64) NOT NULL,
    media_id bigint NOT NULL,
    created_at timestamp without time zone,
    profile_picture character varying(255) DEFAULT NULL::character varying,
    name character varying(255) DEFAULT NULL::character varying,
    url_feed character varying(255) DEFAULT 'NULL::character varying'::character varying,
    url_ref character varying(255) DEFAULT 'NULL::character varying'::character varying,
    is_active boolean DEFAULT true
);


ALTER TABLE public.sources OWNER TO "SMSvc";

--
-- Name: data_cities id; Type: DEFAULT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.data_cities ALTER COLUMN id SET DEFAULT nextval('public.data_cities_id_seq'::regclass);


--
-- Name: media id; Type: DEFAULT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.media ALTER COLUMN id SET DEFAULT nextval('public.media_id_seq'::regclass);


--
-- Name: data_cities data_cities_pkey; Type: CONSTRAINT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.data_cities
    ADD CONSTRAINT data_cities_pkey PRIMARY KEY (id);


--
-- Name: items items_pkey; Type: CONSTRAINT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (stub);


--
-- Name: media media_pkey; Type: CONSTRAINT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: source_locations source_locations_pkey; Type: CONSTRAINT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.source_locations
    ADD CONSTRAINT source_locations_pkey PRIMARY KEY (source_stub);


--
-- Name: sources sources_pkey; Type: CONSTRAINT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (stub);


--
-- Name: sources item_source_to_media_id; Type: FK CONSTRAINT; Schema: public; Owner: SMSvc
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT item_source_to_media_id FOREIGN KEY (media_id) REFERENCES public.media(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

